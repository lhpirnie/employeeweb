package qa.emp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeDAOStub implements EmployeeDAO { // Hand mocking
	private List<Employee> allEmployees;

	public EmployeeDAOStub() {
		allEmployees = getSomeEmployees();
	}
	
	@Override
	public void login(String username, String password) {
		
	}

	@Override
	public List<Employee> getAllEmployees() {
		return new ArrayList<>(allEmployees); // clone for safety!
	}

	@Override
	public void insertEmployee(Employee employee) {
		allEmployees.add(employee);
	}

    private static List<Employee> getSomeEmployees() {
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(new Employee(10001,"Diane","Abbott",44,21000,"ADMIN"));
        employees.add(new Employee(10040,"John","Bercow",63,22000,"ADMIN"));
        employees.add(new Employee(10133,"Jeremy","Corbyn",23,23000,"ADMIN"));
        employees.add(new Employee(10828,"Jon","Cruddas",42,24000,"ADMIN"));
        employees.add(new Employee(25166,"Stephen","Doughty",21,25000,"ADMIN"));
        employees.add(new Employee(24933,"George","Eustice",32,26000,"ADMIN"));
        employees.add(new Employee(11923,"Tim","Farron",32,27000,"ADMIN"));
        employees.add(new Employee(11858,"Michael","Gove",26,28000,"ADMIN"));
        employees.add(new Employee(10257,"Philip","Hammond",19,29000,"ADMIN"));
        employees.add(new Employee(11927,"Stephen","Hammond",65,30000,"ADMIN"));
        employees.add(new Employee(10281,"Margaret","Hodge",56,31000,"ADMIN"));
        employees.add(new Employee(25419,"Ranil","Jayawardena",34,32000,"ADMIN"));
        employees.add(new Employee(24745,"Marcus","Jones",45,33000,"ADMIN"));
        employees.add(new Employee(24816,"Liz","Kendall",44,34000,"ADMIN"));
        employees.add(new Employee(25297,"Stephen","Kinnock",70,35000,"ADMIN"));
        employees.add(new Employee(24829,"Andrea","Leadsom",34,36000,"ADMIN"));
        employees.add(new Employee(11450,"Madeleine","Moon",23,37000,"ADMIN"));
        employees.add(new Employee(24831,"Lisa","Nandy",39,38000,"ADMIN"));
        employees.add(new Employee(25687,"Alex","Norris",37,39000,"ADMIN"));
        employees.add(new Employee(11148,"Albert","Owen",47,40000,"ADMIN"));
        employees.add(new Employee(25617,"Stephanie","Peacock",26,41000,"ADMIN"));
        employees.add(new Employee(24815,"Dominic","Raab",22,42000,"ADMIN"));
        employees.add(new Employee(25685,"Lee","Rowley",18,43000,"ADMIN"));
        employees.add(new Employee(25672,"Laura","Smith",61,44000,"ADMIN"));
        employees.add(new Employee(25345,"Royston","Smith",59,45000,"ADMIN"));
        employees.add(new Employee(10594,"Gareth","Thomas",24,46000,"ADMIN"));
        employees.add(new Employee(10614,"Keith","Vaz",46,47000,"ADMIN"));
        employees.add(new Employee(24862,"Robin","Walker",55,48000,"ADMIN"));
        Collections.shuffle(employees);
        return employees;
    }

}
