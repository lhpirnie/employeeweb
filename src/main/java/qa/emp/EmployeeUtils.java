package qa.emp;

import java.util.List;

public class EmployeeUtils {

    private EmployeeDAO dao;

    public EmployeeUtils(EmployeeDAO dao) {
        this.dao = dao;
    }

    public Employee getHighestPaid() {
        return dao.getAllEmployees().get(0);
    }

    public Employee getLowestPaid() {
        return dao.getAllEmployees().get(0);
    }

    public double getAverageSalary() {
    	double sum = 0.0;
    	List<Employee> emps = dao.getAllEmployees();
    	for (Employee e : emps) {
    		sum += e.getSalary();
    	}
        return sum / emps.size();
    }

}
